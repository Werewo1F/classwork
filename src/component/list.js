import React, {Component} from 'react';
import {connect} from "react-redux";

import Item from "./item";

class List extends Component {

  render() {
    const {list, removeItem, checkItem, addItem} = this.props;
    return (
      <>
        <h1>List</h1>
        <div className="list-wrap">
          {
            (list.length !== 0) ? (
              <>
                {
                  list.map(el => (
                    <Item
                      key={el.id}
                      index={el.id}
                      text={el.name}
                      removeItem={removeItem}
                      checkItem={checkItem}
                    />
                  ))
                }
              </>
            ) : (
              <li>All done</li>
            )
          }
        </div>
        <div>
          <input id="new-task" type="text"/>
          <button onClick={addItem}>Submit</button>
        </div>
      </>
    )
  }
}

/*
* Redux
 */
const stateToProps = (state, ownProps) => ({
  list: state.list
});

const dispatchToProps = (dispatch, ownProps) => ({
  addItem: (e) => {
    dispatch({type: 'ADD_ITEM', payload: document.getElementById('new-task').value})
  },
  removeItem: (e) => {
    dispatch({type: 'REMOVE_ITEM', payload: parseInt(e.target.dataset.key)})
  },
  checkItem: (e) => {
    dispatch({type: 'CHECK_ITEM', payload: parseInt(e.target.dataset.key)})
  }
});


export default connect(stateToProps, dispatchToProps)(List);