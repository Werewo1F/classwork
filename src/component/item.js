import React from 'react';


const Item = ({index, text, removeItem, checkItem}) => {
  return (
    <div className="item">
      {text}
      <input data-key={index} onChange={checkItem} type="checkbox"/>
      <button data-key={index} onClick={removeItem}>Remove</button>
    </div>
  )
}

export default Item;