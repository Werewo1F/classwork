import React, { Component } from 'react';
import {Provider} from 'react-redux';

import store from './redux/store';
import List from './component/list';

import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <List />
        </div>
      </Provider>
    )
  }
}

export default App;
