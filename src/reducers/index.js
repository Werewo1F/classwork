const initialState = {
  list: [
    {
      id: 0,
      done: false,
      name: 'one'
    },
    {
      id: 1,
      done: false,
      name: 'two'
    }
  ]
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case 'REMOVE_ITEM':
      return {
        ...state,
        list: state.list.filter( el => (el.id !== action.payload) ? el : false)
      };

    case 'CHECK_ITEM':
      return {
        ...state,
        list: state.list.map( el => {
          el.done = (el.id === action.payload) ? !el.done : el.done;
          return el;
        })
      }

    case 'ADD_ITEM':
      let newTask = {
        id: state.list.length,
        done: false,
        name: action.payload
      }
      return {
        ...state,
        list: [...state.list, newTask]
      }

    default:
      return state;
  }
}

export default reducer;